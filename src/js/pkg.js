const $ = (q) => document.querySelector(q);

window.addEventListener("hashchange", () => location.reload());

async function getPackages(repo) {
  const repoURL = new URL("Packages.json", repo);
  const repoJson = await fetch(repoURL)
    .then((r) => r.json())
    .catch((e) => {
      throw Error(`Error while fetching ${repo}: ${e}`);
    });

  return repoJson;
}

async function getRepo(repo) {
  const repoURL = new URL("Release.json", repo);
  const repoJson = await fetch(repoURL)
    .then((r) => r.json())
    .catch((e) => {
      throw Error(`Error while fetching ${repo}: ${e}`);
    });

  return repoJson;
}

function formatBytes(bytes, decimals) {
  if (bytes == 0) return "0 Bytes";
  var k = 1024,
    dm = decimals || 2,
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

try {
  const query = new URLSearchParams(location.search.slice(1));
  $("#search").value = query.get("r");

  let url = query.get("r");
  if (!url.endsWith("/")) url += "/";

  let repo = await getRepo(url);
  let pkgs = await getPackages(url);

  if (query.has("pkg")) {
    const pkg = pkgs.find((p) => p.name === query.get("pkg"));
    if (!pkg) throw new Error("The package does not exists");

    const repoURL = new URL(query.get("r"));
    $(".header").innerHTML = `
      <h2>${pkg.friendlyName}</h2>
      <span>${pkg.description}</span>
    `;

    $(".sidebar").innerHTML = `
    <h3>Package Information</h3>

    <ul>
      <li>
        <img no-radius="" src="../src/img/Author.png" alt="Author">
        <span class="pkg-aut">${pkg.author}</span>
      </li>
      <li>
        <img no-radius="" src="../src/img/Version.png" alt="Version">
        <span class="pkg-ver">${pkg.version}</span>
      </li>
      <li>
        <img no-radius="" src="../src/img/Size.png" alt="Size">
        <a href="${repoURL.toString()}">${repoURL.toString()}</a>
      </li>
    </ul>
    `;

    const panel = $(".panel");
    panel.classList.add("sandbox-container");

    const sandbox = document.createElement("iframe");
    sandbox.classList.add("sandbox");

    panel.appendChild(sandbox);

    sandbox.src = `./embed.html?repo=${repoURL.toString()}&pkg=${query.get(
      "pkg"
    )}`;
  } else {
    $(".header").innerHTML = `
      <h2>${repo.name}</h2>
      <span>${repo.maintainers}</span>
    `;

    pkgs.forEach((pkg) => {
      const card = document.createElement("div");
      card.classList.add("kard", "pkg-card");

      console.log(pkg.iconFiles);

      let imgURL = pkg.iconFiles
        ? (pkg.iconFiles["32x32"] || pkg.iconFiles["16x16"]).replaceAll(
            "$REPO_PATH$",
            url.slice(0, -1)
          )
        : "../src/img/icon_l.svg";
      card.innerHTML = `
        <img src="${imgURL}" />
        <b>${pkg.friendlyName}</b>
        <a href="${`${location.search}&pkg=${pkg.name}`}" light class="kard">Try</a>
      `;

      $(".panel").appendChild(card);
    });
  }

  // const pkgsJson = await getPackages(repo.toString());
  // const repoJson = await getRepo(repo.toString());
  // const pkgJson = pkgsJson.find((p) => p.name === pkg);

  // if (!pkgJson) throw new Error(`No package named "${pkg}" on this repository`);

  // ver ||= pkgJson.version;

  // // JSZip
  // let zip = new JSZip();
  // const zipFile = await fetch(
  //   `${repo.toString()}/${pkgJson.rootPath}/content@${ver}.zip`
  // ).then((r) => {
  //   if (!r.ok)
  //     throw new Error(
  //       `The package is not available in this version. The current latest version is "${pkgJson.version}"`
  //     );
  //   return r.blob();
  // });
  // await zip.loadAsync(zipFile);

  // const pkgFile = JSON.parse(await zip.files["pkg.json"].async("text"));
  // const readmeFile = Object.values(zip.files).find(
  //   (f) => f.name.toLowerCase() === "readme.md"
  // );

  // // Infos
  // $(".pkg-frn").innerText = `${pkgFile.frn}`;
  // $(".pkg-info").innerText = `(${pkgJson.name}) · ${pkgFile.ver}`;

  // // Side
  // $(".install-repo").innerText = `kore repo add ${
  //   repoJson.name
  // }=${repo.toString()}`;
  // $(
  //   ".install-pkg"
  // ).innerText = `kore pkg add ${repoJson.name}:${pkgJson.name}@${ver}`;

  // $(".pkg-aut").innerText = pkgFile.aut;
  // $(".pkg-ver").innerText = pkgFile.ver;
  // $(".pkg-size").innerText = formatBytes(zipFile.size);

  // // Markdown preview
  // const readme = readmeFile
  //   ? await readmeFile.async("text")
  //   : "This package does not include a `README.md`";
  // const md = new markdownit({
  //   html: true,
  //   linkify: true,
  //   typographer: true,
  //   highlight: (str, lang) => {
  //     if (lang && hljs.getLanguage(lang)) {
  //       try {
  //         return hljs.highlight(lang, str).value;
  //       } catch (e) {}
  //     }
  //     return "";
  //   },
  // })
  //   .use(window.markdownitEmoji)
  //   .use(window.markdownitSub)
  //   .use(window.markdownitSup)
  //   .use(window.markdownitIns)
  //   .use(window.markdownitMark);

  // $(".readme").innerHTML = md.render(readme);
} catch (e) {
  $(
    ".pkg"
  ).innerHTML = `<h2>Whoops we cannot fetch this repository or package...</h2><br><kard style="color: var(--redow)">${e}</kard>`;
}

$(".pkg").style.display = "grid";

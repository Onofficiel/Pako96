const $ = (q) => document.querySelector(q);

function createPopup(msg, duration) {
  const pop = document.createElement("kard");
  pop.innerText = msg;
  pop.classList.add("info-bubble");

  Object.assign(pop.style, {
    opacity: 0,
    position: "absolute",
    bottom: "16px",
    left: "50%",
    translate: "-50%",
  });

  document.body.querySelectorAll(".info-bubble").forEach((el) => el.remove());
  document.body.appendChild(pop);

  let anim1 = pop.animate(
    [
      { transform: "translateY(5px)", opacity: 0 },
      { transform: "translateY(0)", opacity: 100 },
    ],
    {
      duration: 200,
      endDelay: duration,
      fill: "forwards",
    }
  );

  anim1.addEventListener("finish", async () => {
    await new Promise((r) => setTimeout(r, duration));

    let anim2 = pop.animate(
      [
        { transform: "translateY(0)", opacity: 100 },
        { transform: "translateY(5px)", opacity: 0 },
      ],
      {
        duration: 200,
      }
    );

    anim2.addEventListener("finish", () => {
      pop.remove();
    });
  });
}

$("#search").addEventListener("keydown", ({ key, target }) => {
  if (key !== "Enter") return;

  function error(msg) {
    target.animate(
      [
        { transform: "translateX(0)" },
        { transform: "translateX(5px)" },
        { transform: "translateX(-5px)" },
        { transform: "translateX(5px)" },
        { transform: "translateX(0)" },
      ],
      {
        duration: 100,
        iterations: 5,
      }
    );

    createPopup(msg, 1000);
  }

  const url = target.value;
  if (!url) return error("Please enter a URL");

  if (
    !/^(?:https?:\/\/)([\da-z\.-]+\.[a-z\.]{2,6}|[\d\.]+)([\/:?=&#]{1}[\da-z\.-]+)*[\/\?]?$/i.test(
      url
    )
  ) {
    return error("Invalid URL");
  }

  createPopup("Loading...", 3000);
  fetch(url + "/Packages.json")
    .then((r) => {
      if (r.status === 404) return error("Not a Windows 96 Repository");

      location.href = `?r=${encodeURI(url)}`;
    })
    .catch(() => error("Error while fetching the repository"));
});

//!wrt
const { PkMgr } = w96.sys;

// Get query search
const query = new URLSearchParams(location.search.slice(1));

const repoURL = query.get("pakorepo");
const pkgName = query.get("pakopkg");

// Init Package Manager
const pm = new PkMgr();

// FS Config
await pm.checkFS();
await FS.writestr(
  "C:/system/packages/configs/sources.json",
  JSON.stringify({
    sources: [repoURL],
    updateFrequency: 2,
  })
);

// PkgMgr Setup
await pm.readSources();
await pm
  .reloadPackageCache()
  .catch(() =>
    parent.postMessage(
      [
        "pako-install-info",
        { message: "Not a Windows 96 repository", type: "error" },
      ],
      "*"
    )
  );

// Installing Package
const pkg = pm.getPackage(pkgName);
if (!pkg) {
  parent.postMessage(
    ["pako-install-info", { message: "Package not found", type: "error" }],
    "*"
  );
} else {
  await pm.installPackage(pkg, (msg) => {
    parent.postMessage(["pako-install-info", msg], "*");
  });

  parent.postMessage(["pako-complete"], "*");
}
